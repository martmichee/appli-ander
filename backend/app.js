const express = require('express');
const app = express();
const helmet = require('helmet');
const path = require('path');
const userRoutes = require('./routes/user_route');

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
});


app.use(express.urlencoded({
    extended: true
}))
app.use(express.json());
app.use("/images", express.static(path.join(__dirname, 'images')));

// protection de l'application par rapport à l'entete
app.use(helmet());

// routes
app.use('/api/user', userRoutes);


module.exports = app


