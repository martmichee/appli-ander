const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('../models');
const passwordValidator = require('password-validator');
const mailValidator = require('email-validator');


exports.signup = (req, res, next) => {

       // Propriétés de validation du mots de passe
       const schema = new passwordValidator();
       schema
           .is().min(4)
           .is().max(100)
           .has().uppercase()
           .has().lowercase()
           .has().digits(2)
           .has().not().spaces()
           .is().not().oneOf(['Passw0rd', 'Password123']);
   
           // hashage du mot de passe
       const salt = bcrypt.genSaltSync(10);
       const hash = bcrypt.hashSync(req.body.password, salt);
       const user = {
           firstName: req.body.firstName,
           lastName: req.body.lastName,
           email: req.body.email,
           password: hash
       }
       // test de validité
    //    if (!mailValidator.validate(req.body.email) || (!schema.validate(req.body.password))) {
    //        return res.status(404).json({
    //            error: " invalide !"
    //        })
    //    }
       if (user.firstName == null || user.lastName == null || user.email == null || user.password == null) {
           return res.status(400).json({
               'error': 'champs obligatoire'
           })
       } 
       // création du compte   
       try {
           const newUser =  db.User.create(user);
           return res.status(201).json({
               'message': 'utilisateur créé'
           })
       } catch (error) {
           res.status(500).json({
               error
           })
       }
  };

  exports.login = (req, res, next) => {
    db.User.findOne({
        where: {
            email: req.body.email,
        }
    })
    .then(user => {
        if (!user) {
            return res.status(401).json({
                error: 'Utilisateur inconnu !'
            });
        }           
        // vérification du mots de passe    
        bcrypt.compare(req.body.password, user.password) 
            .then(valid => {
                if (!valid) {
                    return res.status(401).json({ error: 'Mot de passe incorrect !' });
                }
                res.status(200).json({ 
                    userId: user.id, 
                    token: jwt.sign( 
                        { userId: user.id }, 
                        'RANDOM_TOKEN_SECRET', 
                        { expiresIn: '24h' }                            
                        ),                        
                    isAdmin: user.isAdmin ,
                    lastName: user.lastName,
                    firstName: user.firstName,
                    createdAt: user.createdAt
                });
              
            })
            .catch(error => res.status(500).json({ error: error.message }));
    })

    .catch(error => res.status(500).json({ error: error.message }));
  };